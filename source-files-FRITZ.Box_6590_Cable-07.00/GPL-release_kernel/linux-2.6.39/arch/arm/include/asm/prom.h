/*
 * Carsten Langgaard, carstenl@mips.com
 * Copyright (C) 2000 ARM Technologies, Inc.  All rights reserved.
 *
 * ########################################################################
 *
 *  This program is free software; you can distribute it and/or modify it
 *  under the terms of the GNU General Public License (Version 2) as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 *  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 *  for more details.
 *
 *  You should have received a copy of the GNU General Public License along
 *  with this program; if not, write to the Free Software Foundation, Inc.,
 *  59 Temple Place - Suite 330, Boston MA 02111-1307, USA.
 *
 * ########################################################################
 *
 * ARM boards bootprom interface for the Linux kernel.
 *
 */

#ifndef _ARM_PROM_H
#define _ARM_PROM_H

#if 0
extern char *prom_getcmdline(void);
extern void setup_prom_printf(int tty_no);
extern void prom_init_cmdline(void);
extern void prom_meminit(void);
extern void prom_fixup_mem_map(unsigned long start_mem, unsigned long end_mem);
extern unsigned long prom_free_prom_memory (void);
extern void mips_display_message(const char *str);
extern void mips_display_word(unsigned int num);
extern int get_ethernet_addr(char *ethernet_addr);
#endif
#if defined(CONFIG_EARLY_PRINTK)
extern int prom_printf(const char *fmt, ...);
#else/*--- #if defined(CONFIG_EARLY_PRINTK) ---*/
#define prom_printf(arg...)
#endif/*--- #else ---*//*--- #if defined(CONFIG_EARLY_PRINTK) ---*/

extern char *prom_getenv(char *name);

/* Memory descriptor management. */
#define PROM_MAX_PMEMBLOCKS    32
struct prom_pmemblock {
        unsigned long base; /* Within KSEG0. */
        unsigned int size;  /* In bytes. */
        unsigned int type;  /* free or prom memory */
};

/*------------------------------------------------------------------------------------------*\
 * Header WLAN - DECT - Config
\*------------------------------------------------------------------------------------------*/
#define UR8_MAX_CONFIG_ENTRIES  8
#define IKANOS_MAX_CONFIG_ENTRIES  8
#define PUMA_MAX_CONFIG_ENTRIES  8

enum wlan_dect_type {
    WLAN,                /*--- 0 ---*/
    DECT,                /*--- 1 ---*/
    WLAN2,               /*--- 2 ---*/
    ZERTIFIKATE,         /*--- 3 ---*/
    DOCSIS,              /*--- 4 ---*/
    DSL,                 /*--- 5 ---*/
    PROLIFIC,            /*--- 6 ---*/
    WLAN_ZIP,            /*--- 7 ---*/
    WLAN2_ZIP,           /*--- 8 ---*/
    MAX_TYPE,            /*--- 9 ---*/
    EMPTY = 0xFF
};

struct __attribute__ ((packed)) wlan_dect_config {
    unsigned char           Version;        /*--- z.Z. 1 ---*/
    enum  wlan_dect_type    Type :8;        /*--- 0 - WLAN; 1 - DECT ---*/
    unsigned short          Len;            /*--- 384 - WLAN, 128 - DECT ---*/
};

extern void set_wlan_dect_config_address(unsigned int value);
extern int get_wlan_dect_config(enum wlan_dect_type Type, unsigned char *buffer, unsigned int len);
extern int prom_c55_get_base_memory(unsigned int *base, unsigned int *len);

#endif /* !(ARM_PROM_H) */
