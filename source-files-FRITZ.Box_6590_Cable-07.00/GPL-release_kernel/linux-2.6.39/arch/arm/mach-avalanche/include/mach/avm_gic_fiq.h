  /*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2016 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/

#ifndef _AVM_GIC_FIQ_H_
#define _AVM_GIC_FIQ_H_


void avm_gic_fiq_init(void);

unsigned int avm_gic_fiq_nr_ints(void);
void __iomem *avm_gic_fiq_dist_base(void);
void __iomem *avm_gic_fiq_cpu_base(void);

void avm_gic_fiq_enable(unsigned int pinNr, unsigned int cpu);
void avm_gic_fiq_disable(unsigned int pinNr, unsigned int cpu);
#ifdef CONFIG_MACH_PUMA6
void avm_gic_fiq_setup(unsigned int pinNr, unsigned int hif, unsigned int chan, unsigned int type, unsigned int pol);
#else
void avm_gic_fiq_setup(unsigned int pinNr, unsigned int cpu, unsigned int prio, unsigned int config, unsigned int mode);
#endif
void avm_gic_fiq_raise_irq(unsigned int pinNr);
int avm_gic_fiq_is(unsigned int pinNr);
unsigned int avm_gic_fiq_get_ICCHPIR(void);


#endif // #ifndef _AVM_GIC_FIQ_H_
