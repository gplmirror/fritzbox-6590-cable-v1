/*------------------------------------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------------------------------------*/
#ifndef _hw_gpio_h_
#define _hw_gpio_h_

#include <mach/hardware.h>
#include <arch-avalanche/generic/pal.h>


#define _hw_gpio_direction PAL_SYS_GPIO_PIN_DIRECTION_tag
#define _hw_gpio_function  PAL_SYS_GPIO_PIN_MODE_tag

enum _hw_gpio_polarity {
    GPIO_ACTIVE_HIGH = 0,
    GPIO_ACTIVE_LOW  = 1
};

typedef enum _hw_gpio_direction GPIO_DIR;
typedef enum _hw_gpio_function  GPIO_MODE;
typedef enum _hw_gpio_polarity  GPIO_POLAR;


int puma_gpio_ctrl(unsigned int gpio_pin, enum _hw_gpio_function pin_mode, enum _hw_gpio_direction pin_dir);
int puma_gpio_out_bit(unsigned int gpio_pin, int value);
int puma_gpio_out_bit_no_sched(unsigned int gpio_pin, int value);
int puma_gpio_in_bit(unsigned int gpio_pin);



/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#ifdef CONFIG_MACH_PUMA5

#define PUMA_GPIO_BASE      IO_ADDRESS(0x8610900)

/* GPIO registers */
#define GPIO_R_IN           (*(volatile unsigned int *)(PUMA_GPIO_BASE+0x0))
#define GPIO_R_OUT   		(*(volatile unsigned int *)(PUMA_GPIO_BASE+0x4))
#define GPIO_R_DIR   		(*(volatile unsigned int *)(PUMA_GPIO_BASE+0x8))     /* 0=output, 1=input */
#define GPIO_R_EN    		(*(volatile unsigned int *)(PUMA_GPIO_BASE+0xc))     /* 0=Func 1=GPIO */

#define GPIO_R_AUX_IN       (*(volatile unsigned int *)(PUMA_GPIO_BASE+0x28))
#define GPIO_R_AUX_OUT   	(*(volatile unsigned int *)(PUMA_GPIO_BASE+0x2C))
#define GPIO_R_AUX_DIR   	(*(volatile unsigned int *)(PUMA_GPIO_BASE+0x30))     /* 0=output, 1=input */
#define GPIO_R_AUX_EN    	(*(volatile unsigned int *)(PUMA_GPIO_BASE+0x34))     /* 0=Func 1=GPIO */


#define PUMA_GPIO_AS_OUTPUT      0
#define PUMA_GPIO_AS_INPUT       1
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline void PUMA_GPIO_SETDIR(int gpio, int mode) {
    if(gpio < 32) {
        if(mode)  GPIO_R_DIR |= (1U << gpio); else GPIO_R_DIR &= ~(1U << gpio);
    } else {
        gpio -= 32;
        if(mode) GPIO_R_AUX_DIR |= (1U << gpio); else GPIO_R_AUX_DIR &= ~(1U << gpio);
    }
}
/*--- Achtung! Aenderung zum UR8! Auch im "Functionmode" muss der GPIO enabled werden !? So zumindest Erfahrung bei TDM-Pins ---*/
#define PUMA_GPIO_OFF      0
#define PUMA_GPIO_ON       1
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline void PUMA_GPIO_ENABLE(int gpio, int mode) {
    if(gpio < 32) {
        if(mode)  GPIO_R_EN |= (1U << gpio); else  GPIO_R_EN &= ~(1U << gpio);
    } else {
        gpio -= 32;
        if(mode)  GPIO_R_AUX_EN |= (1U << gpio); else GPIO_R_AUX_EN &= ~(1U << gpio);
    }
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline int PUMA_GPIO_INPUT(int gpio) {
    if(gpio < 32) {
        return (GPIO_R_IN & (1U << gpio)) ? 1 : 0;
    }
    gpio -= 32;
    return (GPIO_R_AUX_IN & (1U << gpio)) ? 1 : 0;
}
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static inline void PUMA_GPIO_OUTPUT(int gpio, int val) {
    if(gpio < 32) {
        if(val) GPIO_R_OUT |= (1U << gpio); else GPIO_R_OUT &= ~(1U << gpio);
    } else {
        gpio -= 32;
        if(val) GPIO_R_AUX_OUT |= (1U << gpio); else GPIO_R_AUX_OUT &= ~(1U << gpio);
    }
}

/*--- Muxing GPIO's ---*/
#define GPIO_TDM_CLK        18
#define GPIO_TDM_FS         19
#define GPIO_TDM_RX         20
#define GPIO_CODEC_CLK      21
#define GPIO_CODEC_DATA     22
#define GPIO_CODEC_CS1_N    23
#define GPIO_CODEC_RESET0   24
#define GPIO_CODEC_RESET1   25
#define GPIO_EINT0          26
#define GPIO_EINT1          27
#define GPIO_PWM            28
#define GPIO_OOB_ITX        29
#define GPIO_OOB_QTX        30
#define GPIO_CLK_OUT1       31

#define _GPIO_AUX           32

#define GPIO_ASR_CS1_N      (_GPIO_AUX + 0)
#define GPIO_ASR_WAIT_N     (_GPIO_AUX + 1) 
#define GPIO_ASR_D0         (_GPIO_AUX + 2)
#define GPIO_ASR_D1         (_GPIO_AUX + 3)
#define GPIO_ASR_D2         (_GPIO_AUX + 4)
#define GPIO_ASR_D3         (_GPIO_AUX + 5)
#define GPIO_ASR_D4         (_GPIO_AUX + 6)
#define GPIO_ASR_D5         (_GPIO_AUX + 7)
#define GPIO_ASR_D6         (_GPIO_AUX + 8)
#define GPIO_ASR_D7         (_GPIO_AUX + 9)
#define GPIO_VLYNQ_SCRUN    (_GPIO_AUX + 10)
#define GPIO_VLYNQ_RXD0     (_GPIO_AUX + 11)
#define GPIO_VLYNQ_RXD1     (_GPIO_AUX + 12)
#define GPIO_VLYNQ_RXD2     (_GPIO_AUX + 13)
#define GPIO_VLYNQ_RXD3     (_GPIO_AUX + 14)
#define GPIO_VLYNQ_TXD0     (_GPIO_AUX + 15)
#define GPIO_VLYNQ_TXD1     (_GPIO_AUX + 16)
#define GPIO_VLYNQ_TXD2     (_GPIO_AUX + 17)
#define GPIO_VLYNQ_TXD3     (_GPIO_AUX + 18)
#define GPIO_GMII_TD4       (_GPIO_AUX + 19)
#define GPIO_GMII_TD5       (_GPIO_AUX + 20)
#define GPIO_GMII_TD6       (_GPIO_AUX + 21)
#define GPIO_GMII_TD7       (_GPIO_AUX + 22)
#define GPIO_GMII_RD4       (_GPIO_AUX + 23)
#define GPIO_GMII_RD5       (_GPIO_AUX + 24)
#define GPIO_GMII_RD6       (_GPIO_AUX + 25)
#define GPIO_GMII_RD7       (_GPIO_AUX + 26)
#define GPIO_AGCS2          (_GPIO_AUX + 27)
#define GPIO_AGCS3          (_GPIO_AUX + 28)
#define GPIO_AGCS4          (_GPIO_AUX + 29)
#define GPIO_TAGCS2         (_GPIO_AUX + 30)
#define GPIO_TAGCS3         (_GPIO_AUX + 31)

#endif /*--- #ifdef CONFIG_MACH_PUMA5 ---*/


#endif/*--- #ifndef _hw_gpio_h_ ---*/
