#ifndef __hw_timer_h__
#define __hw_timer_h__

#include <mach/hardware.h>

#if defined(CONFIG_ARCH_PUMA5)
#include <arch-avalanche/puma5/puma5.h>
#elif defined(CONFIG_MACH_PUMA6)
#include <arch-avalanche/puma6/puma6.h>
#endif

#define BIG_ENDIAN

#define PUMA_TIMER0_BASE      AVALANCHE_TIMER0_BASE
#define PUMA_TIMER1_BASE      AVALANCHE_TIMER1_BASE
#define PUMA_TIMER2_BASE      AVALANCHE_TIMER2_BASE
#define PUMA_WDT_BASE         AVALANCHE_WATCHDOG_TIMER_BASE

struct _timer_hw {
    union __timer_control {
        volatile unsigned int reg;
        volatile struct _timer_control {
#ifdef LITTLE_ENDIAN
            unsigned int go : 1;
            unsigned int mode : 1;
            unsigned int prescale : 4;
            unsigned int reserved1 : 9;
            unsigned int prescale_enable : 1;
            unsigned int reserved2 : 16;
#endif/*--- #ifdef LITTLE_ENDIAN ---*/
#ifdef BIG_ENDIAN
            unsigned int reserved2 : 16;
            unsigned int prescale_enable : 1;
            unsigned int reserved1 : 9;
            unsigned int prescale : 4;
            unsigned int mode : 1;
            unsigned int go : 1;
#endif/*--- #ifdef BIG_ENDIAN ---*/
        } Bits;
    } timer_control;
    volatile unsigned int timer_load_reg;
    volatile unsigned int timer_value_reg;
    volatile unsigned int timer_interrupt_reg;
};

#endif/*--- #ifndef __hw_timer_h__ ---*/
