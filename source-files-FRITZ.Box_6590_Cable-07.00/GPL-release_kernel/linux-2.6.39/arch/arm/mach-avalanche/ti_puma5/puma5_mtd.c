/*
 * ----------------------------------------------------------------------------
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * ----------------------------------------------------------------------------
 *
 */

/**************************************************************************
 * Included Files
 **************************************************************************/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/major.h>
#include <linux/root_dev.h>
#include <linux/dma-mapping.h>
#include <linux/platform_device.h>
#include <linux/env.h>

#include <asm/setup.h>
#include <asm/io.h>

#if defined(CONFIG_MTD_PHYSMAP) || defined(CONFIG_MTD_PHYSMAP_MODULE) || defined(CONFIG_ARCH_PUMA5)
#define DO_MTD

#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>
#include <linux/mtd/physmap.h>
#include <linux/mtd/plat-ram.h>
#include <linux/squashfs_fs.h>
#include <linux/jffs2.h>


#define PUMA5_MTD_DEBUG

#if defined(PUMA5_MTD_DEBUG)
#define DEBUG_MTD(fmt,arg...)  printk("[%s](%u): " fmt ,__FUNCTION__, __LINE__, ##arg);
#else
#define DEBUG_MTD(fmt,arg...)
#endif

#define MAX_FLASH_MTD   7
#define PUMA5_MTD_JFFS2_MIN_SIZE        6
#define PUMA5_MTD_JFFS2_MAX_SIZE       ((1 << 20) >> 16)


#define MAX_MTD_DEVICES             (MAX_FLASH_MTD + 5)

static struct mtd_partition puma5_partitions[MAX_FLASH_MTD];
static struct mtd_partition puma5_partitions2[5];
static unsigned int linux_fs_start = 0;
/*-------------------------------------------------------------------------------------*\
 * Zuerst wird das JFFS2 gesucht, dann das Squash-FS!
\*-------------------------------------------------------------------------------------*/
static const char *probes[] = { "find_squashfs" , NULL };
static unsigned int flash_erase_block_size = 0;

static struct physmap_flash_data puma5_flash_data = {
	.width		= 2,
	.parts		= puma5_partitions,
	.nr_parts	= ARRAY_SIZE(puma5_partitions),
    .probe_type = "find_squashfs"
};

static struct physmap_flash_data puma5_flash_data2 = {
	.width		= 2,
	.parts		= puma5_partitions2,
	.nr_parts	= ARRAY_SIZE(puma5_partitions2),
    .probe_type = "find_squashfs"
};

/* NOTE: CFI probe will correctly detect flash part as 32M, but EMIF
 * limits addresses to 16M, so using addresses past 16M will wrap */
static struct resource puma5_flash_resource[3] = {
    {
        .start		= 0x48000000,
        .end		= 0x48000000 + (16 << 20),    /* 16 MB */
        .flags		= IORESOURCE_MEM,
    },
    {   
        .start		= 0x4C000000,
        .end		= 0x4C000000 + (16 << 20),
        .flags		= IORESOURCE_MEM,
    },
    {   /* f�r ins RAM geladenes Filesystem */ 
        .start		= 0,       /*--- werden beim Aufsetzen ausgef�llt ---*/
        .end		= 0 + (16 << 20),
        .flags		= IORESOURCE_MEM,
    }
};

void puma5_ram_mtd_set_rw(struct device *pdev, int);
extern int __init root_dev_setup(char *line);

#ifdef CONFIG_MTD_SPI
struct platform_device puma5_spiflash_device[2] = {
    {
        .name		= "macronix",
        .id		    = 0,
        .dev		= {
            .platform_data	= &puma5_flash_data,
        },
        .num_resources	= 1,
        .resource	= &puma5_flash_resource[0],
    },
    {
        .name		= "macronix",
        .id		    = 1,
        .dev		= {
            .platform_data	= &puma5_flash_data2,
        },
        .num_resources	= 1,
        .resource	= &puma5_flash_resource[1],
    }
};
#endif

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static struct mtd_partition puma5_ram_partitions[3];

static struct platdata_mtd_ram puma5_ram_data = {
	.mapname       = "ram-filesystem",
	.bankwidth	   = 4,
	.partitions    = puma5_ram_partitions,
	/*--- .nr_partitions = ARRAY_SIZE(puma5_ram_partitions), ---*/
    .set_rw        = puma5_ram_mtd_set_rw,
    .probes        = probes
};

struct platform_device puma5_ram_device = {
	.name		= "mtd-ram",
	.id		    = -1,
	.dev		= {
		.platform_data	= &puma5_ram_data,
	},
	.num_resources	= 1,
	.resource	= &puma5_flash_resource[2],
};

static unsigned int puma5_max_plattforms = 2;       /*--- 2 Platformdevices - 2 spi-Flashs ---*/
struct platform_device *puma5_platform_devices[] = {
#ifdef CONFIG_MTD_SPI
    &puma5_spiflash_device[0],
    &puma5_spiflash_device[1],
#endif
    &puma5_ram_device 
};

/*-------------------------------------------------------------------------------------*\
\*-------------------------------------------------------------------------------------*/
void puma5_init_platform_devices(void) {
	platform_add_devices(puma5_platform_devices, puma5_max_plattforms);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void puma5_ram_mtd_set_rw(struct device *pdev, int mode) {
    if(mode == PLATRAM_RO) {
        DEBUG_MTD("PLATRAM_RO\n");
    } else if(mode == PLATRAM_RW) {
        DEBUG_MTD("PLATRAM_RW\n");
    }
}

enum _flash_map_enum {
    MAP_UNKNOWN,
    MAP_RAM,
    MAP_FLASH
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#define pointer_fail_strcmp(a, b) __pointer_fail_strcmp(a, b, __FUNCTION__, __LINE__) 
int __pointer_fail_strcmp(char *ptr, const char *cmp, const char *func, int line) {
    if(!ptr || !cmp) {
        printk(KERN_ERR "[%s] line %d strcmp(%pF, %pF)\n", func, line, ptr, cmp);
        return -1;
    }
    return strcmp(ptr, cmp);
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int puma5_squashfs_parser_function(struct mtd_info *mtd, struct mtd_partition **p_mtd_pat, unsigned long param) {
    enum _flash_map_enum maptype = MAP_UNKNOWN;
    unsigned count = 1, maxcount = 0;
    
    DEBUG_MTD("mtd_info->name=%s, mtd_info->index=%u, param=%lu, p_mtd_pat=0x%p\n", mtd->name, mtd->index, param, p_mtd_pat);
    
    if (!pointer_fail_strcmp(mtd->name, "ram-filesystem")) {
        maptype = MAP_RAM;
    } else if (!pointer_fail_strcmp(mtd->name, "macronix") ) {
        DEBUG_MTD("Detected macronix\n");
        maptype = MAP_FLASH;
        flash_erase_block_size = mtd->erasesize;
    } else {
        DEBUG_MTD("with unknown mtd type %s\n", mtd->name);
        return 0;
    }

    if(p_mtd_pat) {
        unsigned int magic = 0, readlen = 0;
        loff_t pos, start_offset;

        if(*p_mtd_pat) 
            DEBUG_MTD("*p_mtd_pat->name %s\n", (*p_mtd_pat)->name);

        switch (maptype) {
            case MAP_FLASH:
                DEBUG_MTD("MAP_FLASH\n");
                switch(param) {
                    case 0: /*--- chip select 0 ---*/
                        if(*p_mtd_pat == NULL) {
                            *p_mtd_pat = puma5_partitions;
                        }
                        maxcount = ARRAY_SIZE(puma5_partitions);
                        break;
                    case 1: /*--- chip select 1 ---*/
                        if(*p_mtd_pat == NULL) {
                            *p_mtd_pat = puma5_partitions2;
                        }
                        maxcount = ARRAY_SIZE(puma5_partitions2);
                        break;
                    default:
                        maxcount = 0;
                        break;
                }
                break;
            case MAP_RAM:
                DEBUG_MTD("MAP_RAM\n");
                if(*p_mtd_pat == NULL) {
                    *p_mtd_pat = puma5_ram_partitions;
                }
                maxcount = ARRAY_SIZE(puma5_ram_partitions);
                break;
            default:
                break;
        }
#if defined(PUMA5_MTD_DEBUG)
        {
            int i;
            for(i = 0 ; i < maxcount ; i++)
                DEBUG_MTD("[mtd%d] %20s: 0x%08llx - 0x%08llx (size 0x%u)\n",
                        i,
                        (*p_mtd_pat)[i].name,
                        (*p_mtd_pat)[i].offset,
                        (*p_mtd_pat)[i].offset + (*p_mtd_pat)[i].size,
                        (*p_mtd_pat)[i].size);
        }
#endif

        DEBUG_MTD("try partition=%s (offset=0x%llx, len=%llx, blocksize=%x)\n", 
                (*p_mtd_pat)[count].name,
                (*p_mtd_pat)[count].offset,
                (*p_mtd_pat)[count].size,
                mtd->erasesize);

        start_offset = pos = (*p_mtd_pat)[count].offset;
        while(pos < (*p_mtd_pat)[1].offset + (*p_mtd_pat)[count].size) {
            mtd->read(mtd, pos, sizeof(unsigned int), &readlen, (u_char*)&magic);
            /*--- DEBUG_MTD("read %u bytes, magic = 0x%08x, index=%u, pos=0x%Lx\n", readlen, magic, mtd->index, pos); ---*/
            if((magic == SQUASHFS_MAGIC) || (magic == SQUASHFS_MAGIC_SWAP) ) {

                (*p_mtd_pat)[0].offset = pos;
                (*p_mtd_pat)[0].size	 = (u_int32_t)start_offset + (u_int32_t)(*p_mtd_pat)[1].size - (u_int32_t)pos;
                if (maptype == MAP_RAM) {
                    (*p_mtd_pat)[0].name	 = "filesystem_ram";
                } else {
                    (*p_mtd_pat)[0].name	 = (linux_fs_start == param) ? "filesystem" : "filesystem_reserved";
                }
                (*p_mtd_pat)[1].size	 = (u_int32_t)pos - (u_int32_t)start_offset;
                if (maptype == MAP_RAM) {
                    (*p_mtd_pat)[1].name     = "kernel_ram";
                } else {
                    (*p_mtd_pat)[1].name     = (linux_fs_start == param) ? "kernel" : "kernel_reserved";
                }
                /*--------------------------------------------------------------------------*\
                 * JFFS2 aufbereiten
                \*--------------------------------------------------------------------------*/
                if (maptype == MAP_FLASH) {
                    int jffs_part;
                    char *p;
                    u_int32_t   jffs2_size, jffs2_start, jffs2_earliest_start, urlader_jff2_size;
                    for(jffs_part = 0 ; jffs_part < maxcount ; jffs_part++) {
                        if((*p_mtd_pat)[jffs_part].name && !strncmp((*p_mtd_pat)[jffs_part].name, "jffs2", 5)) {
                            printk(KERN_ERR "use partition %d (%s) for jffs2\n", jffs_part, (*p_mtd_pat)[jffs_part].name);
                            break;
                        }
                    }
                    if(jffs_part < maxcount) {
                        unsigned int jffs_size_changed = 0;
                        /* JFFS2 nicht gefunden: Wenn jffs2_size gesetzt ist, ggf. verkleinern */
                        /* sonst anlegen mit der verbleibenden Flash Gr�sse nach Filesystem % 64k */
                        struct squashfs_super_block squashfs_sb;
                        unsigned int erasesize = mtd->erasesize;

                        if(erasesize < (1 << 16)) erasesize = (1 << 16);

                        urlader_jff2_size = 0;
                        p = prom_getenv((char*)"jffs2_size");
                        if(p) {
                            urlader_jff2_size = (uint32_t)simple_strtoul(p, NULL, 10);
                        } else {
                            jffs_size_changed = 1;
                            printk(KERN_ERR "[%s] jffs2_size not set.\n", __FUNCTION__);
                        }

                        mtd->read(mtd, (loff_t)pos, sizeof(struct squashfs_super_block), &readlen, (u_char*)&squashfs_sb);
                        jffs2_earliest_start = (u_int32_t)pos + (u_int32_t)squashfs_sb.bytes_used;
                        /*--- printk("squashfs pos: %x\n", (u_int32_t)pos); ---*/
                        /*--- printk("squashfs size: %x\n", (u_int32_t)squashfs_sb.bytes_used); ---*/
                        /*--- printk("jffs2_start (squashfs pos + len) = %x\n", (u_int32_t)jffs2_earliest_start); ---*/
                        if (jffs2_earliest_start & (erasesize - 1)) {
                            /*--- printk("align jffs: start: %x\n", jffs2_earliest_start); ---*/
                            jffs2_earliest_start = (jffs2_earliest_start & ~(erasesize - 1)) + erasesize;
                        }
                        /*--- printk("jffs2_earliest_start (aligned) = %x\n", jffs2_earliest_start); ---*/
                        jffs2_size = ((*p_mtd_pat)[0].offset + (*p_mtd_pat)[0].size - jffs2_earliest_start) >> 16;
                        /* jffs2_size in 64k Bl�cken. Muss ggf. um 1 veringert werden f�r 128k Block Flash */
                        /*--- printk("jffs2_size = %x\n", jffs2_size); ---*/
                        jffs2_size = jffs2_size & ~((erasesize / 0x10000)-1);
                        /*--- printk("jffs2_size = %x\n", jffs2_size); ---*/

                        if (jffs2_size < (PUMA5_MTD_JFFS2_MIN_SIZE * (erasesize / 0x10000))) {
                            printk(KERN_WARNING "[%s]: not enough space for JFFS2!\n", __FUNCTION__);
                        } else {
                            if (urlader_jff2_size == 0) {
                                /* F�r 7320 ohne JFFS_SIZE im Urlader-Env. die Gr��e
                                 * auf 16 begrenzen und nach hinten schieben, damit nicht bei jedem FW Update das
                                 * JFFS �berschrieben wird */
                                if (jffs2_size > PUMA5_MTD_JFFS2_MAX_SIZE) {
                                    printk(KERN_WARNING "[%s]: limiting jffs2_size to %d\n", __FUNCTION__, PUMA5_MTD_JFFS2_MAX_SIZE);
                                    jffs2_start = jffs2_earliest_start + (jffs2_size - PUMA5_MTD_JFFS2_MAX_SIZE) * 0x10000;
                                    jffs2_size = PUMA5_MTD_JFFS2_MAX_SIZE;
                                    jffs_size_changed = 1;
                                } else {
                                    jffs2_start = jffs2_earliest_start;
                                }
                            } else {
                                /* jffs2_size aus dem Urlader verwenden */
                                if (jffs2_size > urlader_jff2_size) {
                                    if (urlader_jff2_size < (PUMA5_MTD_JFFS2_MIN_SIZE * (erasesize / 0x10000))) {
                                        urlader_jff2_size = (PUMA5_MTD_JFFS2_MIN_SIZE * (erasesize / 0x10000));
                                        printk(KERN_WARNING "[%s]: jffs2_size too small, use %d\n", __FUNCTION__, urlader_jff2_size);
                                        jffs_size_changed = 1;
                                    }
                                    jffs2_start = jffs2_earliest_start + (jffs2_size - urlader_jff2_size) * 0x10000;
                                    jffs2_size = urlader_jff2_size;
                                } else {
                                    if(jffs2_size < urlader_jff2_size) {
                                        jffs_size_changed = 1;
                                    }
                                    jffs2_start = jffs2_earliest_start;
                                }
                            }
                            (*p_mtd_pat)[jffs_part].offset = jffs2_start;
                            (*p_mtd_pat)[jffs_part].size   = jffs2_size * 0x10000;
                            (*p_mtd_pat)[0].size	      -= jffs2_size * 0x10000;  /*--- File System partition verkleinern ---*/

                            /*--- printk("[puma5_squashfs_parser_function] jffs2_start@%x size: %d\n", jffs2_start, jffs2_size); ---*/ 
                            if(jffs_size_changed) {
                                struct erase_info instr;
                                int ret;
                            printk(KERN_ERR "[%s] JFFS2 size changed, erase old filesystem\n", __FUNCTION__);

                                memset(&instr, 0, sizeof(instr));
                                instr.mtd = mtd;
                                instr.addr = jffs2_start;
                                instr.len = jffs2_size * 0x10000;
                                instr.callback = NULL;
                                instr.fail_addr = 0xffffffff;

                                ret = mtd->erase(mtd, &instr);
                                if (ret) {
                                    printk(KERN_ERR "jffs mtd erase failed %d\n", ret);
                                }
                            }
                        }
                    }
                }
                DEBUG_MTD("magic found @pos 0x%x\n" , (unsigned int)pos);
                return maxcount;
            }
            pos += 256;
        }
        return maxcount;
        
    }
    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
unsigned int get_erase_block_size_on_ram_device(struct mtd_info *mtd) {
    unsigned int readlen = 0;
    loff_t pos = 0;
    unsigned int value1, value2;

    mtd->read(mtd, pos, sizeof(unsigned int), &readlen, (u_char*)&value1);
    if(readlen != sizeof(unsigned int))
        return 0;
    /*--- DEBUG_MTD("name=%s pos=0x%x value=0x%x\n" , mtd->name, pos, value1); ---*/

    pos += 0x10000ULL;
    mtd->read(mtd, pos, sizeof(unsigned int), &readlen, (u_char*)&value2);
    if(readlen != sizeof(unsigned int))
        return 0;
    /*--- DEBUG_MTD("name=%s pos=0x%x value2=0x%x\n" , mtd->name, pos, value2); ---*/

    if(value1 == value2) {
        pos += 0x10000ULL;
        mtd->read(mtd, pos, sizeof(unsigned int), &readlen, (u_char*)&value2);
        if(readlen != sizeof(unsigned int))
            return 0;
        /*--- DEBUG_MTD("name=%s pos=0x%x value2=0x%x (check)\n" , mtd->name, pos, value2); ---*/

        if(value1 == value2) {
            DEBUG_MTD("eraseblocksize=0x10000\n");
            return 0x10000;
        }
        return 0;
    }

    pos += 0x10000ULL;
    mtd->read(mtd, pos, sizeof(unsigned int), &readlen, (u_char*)&value2);
    if(readlen != sizeof(unsigned int))
        return 0;
    DEBUG_MTD("name=%s pos=0x%Lx value2=0x%x\n", mtd->name, pos, value2);

    if(value1 == value2) {
        DEBUG_MTD("eraseblocksize=0x20000\n" );
        return 0x20000;
    }
    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
struct mtd_part_parser puma5_squashfs_parser = {
	.name     = "find_squashfs",
	.parse_fn = puma5_squashfs_parser_function
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int get_partition_index(struct mtd_info *mtd) {

    /*--- extern struct mtd_info *mtd_table[MAX_MTD_DEVICES]; ---*/
    /*--- unsigned int i; ---*/

    /*--- for(i = 0 ; i < MAX_MTD_DEVICES ; i++) { ---*/
        /*--- if(mtd_table[i] == mtd) { ---*/
            /*--- return i; ---*/
        /*--- } ---*/
    /*--- } ---*/
    /*--- return -1; ---*/

    return mtd->index;
} 

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
/*--- extern struct mtd_info *mtd_table[MAX_MTD_DEVICES]; ---*/
extern int tffs_mtd[2];
extern int tffs_mtd_offset[2];
static int found_rootfs_ram = 0;
char *str_rootfs[2] = { "filesystem_ram", "filesystem" };
struct mtd_info *puma5_urlader_mtd;
void puma5_mtd_add_notifier(struct mtd_info *mtd) {

    int i, index;

    DEBUG_MTD("name %s\n" , mtd->name);

    for (i=0;i<2;i++) {
        if (!pointer_fail_strcmp(mtd->name, str_rootfs[i])) {
            DEBUG_MTD("found %s\n", mtd->name);
            if (found_rootfs_ram)       /*--- we found a rootfs in RAM and use only this ---*/
                return;
            if (!pointer_fail_strcmp(mtd->name, str_rootfs[0]))
                found_rootfs_ram = 1;   /*--- signal that we found a rootfs in RAM ---*/

            index = get_partition_index(mtd);
            DEBUG_MTD("use %s\n" , mtd->name);
            if (index >= 0) {
                static char root_device[64];
                sprintf(root_device, "/dev/mtdblock%d", index);
                DEBUG_MTD("root device: %s (%s)\n" , root_device, mtd->name);
                /*--- DEBUG_MTD("root device: %s (%s)\n" , root_device, mtd_table[index]->name); ---*/
                root_dev_setup(root_device);
                return;
            } else {
                /*--- DEBUG_MTD("%s is not my root device\n" , mtd_table[index] ? mtd_table[index]->name : "<NULL>"); ---*/
                DEBUG_MTD("%s is not my root device\n" , mtd && mtd->name ? mtd->name : "<NULL>");
            }
        }
    }

    if(!pointer_fail_strcmp(mtd->name, "ram-jffs2")) {
        mtd->erasesize = get_erase_block_size_on_ram_device(mtd);
        if(mtd->erasesize == 0)
            mtd->erasesize = flash_erase_block_size;
        DEBUG_MTD("%s: set erasesize to 0x%x\n" , mtd->name, flash_erase_block_size);
    } else if(!pointer_fail_strcmp(mtd->name, "ram-filesystem")) {
        mtd->erasesize = get_erase_block_size_on_ram_device(mtd);
        if(mtd->erasesize == 0)
            mtd->erasesize = flash_erase_block_size;
        DEBUG_MTD("%s: set erasesize to 0x%x\n" , mtd->name, flash_erase_block_size);
    } else if(!pointer_fail_strcmp(mtd->name, "tffs (1)")) {
        index = get_partition_index(mtd);
        if (index >= 0) {
            tffs_mtd[0] = index;
            DEBUG_MTD("tffs (1) on Index %d\n", index);
        }
    } else if(!pointer_fail_strcmp(mtd->name, "tffs (2)")) {
        index = get_partition_index(mtd);
        if (index >= 0) {
            tffs_mtd[1] = index;
            DEBUG_MTD("tffs (1) on Index %d\n", index);
        }
    } else {
        DEBUG_MTD("skip %s\n" , mtd->name);
    }
    if(!pointer_fail_strcmp(mtd->name, "urlader")) {
        DEBUG_MTD("set puma5_urlader_mtd\n");
        puma5_urlader_mtd = mtd;
    }
}

void puma5_mtd_rm_notifier(struct mtd_info *mtd) {
    DEBUG_MTD("[puma5_mtd_rm_notifier] ignore %s\n", mtd->name);
}

struct mtd_notifier puma5_mtd_notifier = {
    add: puma5_mtd_add_notifier,
    remove: puma5_mtd_rm_notifier
};

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int __init puma5_mtd_init(void) {

    char *p;
    struct _my_mtd {
        uint64_t start, end;
        char *Name;
    } mtd[MAX_MTD_DEVICES];
    unsigned int i; 
     
    DEBUG_MTD("\n");

    p = prom_getenv("linux_fs_start");
    if (p) {
        if (!pointer_fail_strcmp(p, "0"))
            linux_fs_start = 0;
        else if (!pointer_fail_strcmp(p, "1"))
            linux_fs_start = 1;
        else if (!pointer_fail_strcmp(p, "nfs"))
            linux_fs_start = 2;
    }

    DEBUG_MTD("linux_fs_start %d\n", linux_fs_start);

    memset(&mtd[0], 0, sizeof(mtd));

    for(i = 0 ; i < MAX_FLASH_MTD-1 ; i++) {
        switch(i) {
            case 0:
                p = prom_getenv("mtd0");
                if (linux_fs_start == 0)
                    mtd[i].Name = (char*)"filesystem";
                else
                    mtd[i].Name = (char*)"filesystem_reserved";
                break;
            case 1:
                p = prom_getenv("mtd1");
                if (linux_fs_start == 0)
                    mtd[i].Name = (char*)"kernel";
                else
                    mtd[i].Name = (char*)"kernel_reserved";
                break;
            case 2:
                p = prom_getenv("mtd2");
                mtd[i].Name = (char*)"urlader";
                break;
            case 3:
                p = prom_getenv("mtd3");
                mtd[i].Name = (char*)"tffs (1)";
                break;
            case 4:
                p = prom_getenv("mtd4");
                mtd[i].Name = (char*)"tffs (2)";
                break;
        }
        if(p == NULL) {
            continue;
        }
        DEBUG_MTD("mtd[%u] = %s\n" , i, p);
        mtd[i].start  = (uint64_t)simple_strtoul(p, NULL, 16);
        mtd[i].start &= ~0x4C000000;
        p = strchr(p, ',');
        if(p == NULL) {
            mtd[i].start = 0;
            continue;
        }
        p++;
        mtd[i].end  = (uint64_t)simple_strtoul(p, NULL, 16);
        mtd[i].end &= ~0x4C000000;
        DEBUG_MTD("mtd[%u] = 0x%08llx - 0x%08llx\n" , i, mtd[i].start, mtd[i].end);

    }
    for(i = 0 ; i < MAX_FLASH_MTD-1 ; i++) {
        puma5_partitions[i].name	    = mtd[i].Name;
        puma5_partitions[i].offset	    = mtd[i].start;
        puma5_partitions[i].size	    = mtd[i].end - mtd[i].start;
        puma5_partitions[i].mask_flags  = 0;
    }
    
    /*--- MTD-Offsets f�r Adressierung = MTD-Offset[3/4] - Urlader-Offset ---*/ 
    tffs_mtd_offset[0] = puma5_partitions[3].offset - puma5_partitions[2].offset;
    tffs_mtd_offset[1] = puma5_partitions[4].offset - puma5_partitions[2].offset;

    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
    if (linux_fs_start == 0) {
        puma5_partitions[MAX_FLASH_MTD-2].name   = "extra";    /*--- Kernel und Filesystem zusammen ---*/
        puma5_partitions[MAX_FLASH_MTD-1].name   = "jffs2";    /*--- Kernel und Filesystem zusammen ---*/
        puma5_partitions2[4].name  = "jffs2_reserved";     /*--- Kernel und Filesystem zusammen ---*/
    } else {
        puma5_partitions2[4].name  = "jffs2";     /*--- Kernel und Filesystem zusammen ---*/
        puma5_partitions[MAX_FLASH_MTD-2].name   = "extra_reserved";    /*--- Kernel und Filesystem zusammen ---*/
        puma5_partitions[MAX_FLASH_MTD-1].name   = "jffs2_reserved";    /*--- Kernel und Filesystem zusammen ---*/
    }

    puma5_partitions[MAX_FLASH_MTD-2].offset = puma5_partitions[1].offset;
    puma5_partitions[MAX_FLASH_MTD-2].size   = puma5_partitions[1].size;
    puma5_partitions[MAX_FLASH_MTD-2].mask_flags = 0;

    {
        int i;
        for(i = 0 ; i < MAX_FLASH_MTD ; i++)
            DEBUG_MTD("mtd%d: %20s: 0x%08llx - 0x%08llx (size 0x%llx)\n" ,
                    i,
                    puma5_partitions[i].name,
                    puma5_partitions[i].offset,
                    puma5_partitions[i].offset + puma5_partitions[i].size,
                    puma5_partitions[i].size);
    }

    /*--------------------------------------------------------------------------------------*\
     * Config space im zweiten SPI Flash
    \*--------------------------------------------------------------------------------------*/
    puma5_partitions2[2].name   = "config-space";
    puma5_partitions2[2].offset = 0;
    puma5_partitions2[2].size   = 0 
                                  + puma5_partitions[2].size  /*--- urlader ---*/
                                  + puma5_partitions[3].size  /*--- tffs 1 ---*/
                                  + puma5_partitions[4].size  /*--- tffs 2 ---*/
                                  ;
    puma5_partitions2[2].mask_flags = 0;

    /*--------------------------------------------------------------------------------------*\
     * dummy filesystem image am anfang f�r den Parser
    \*--------------------------------------------------------------------------------------*/
    if (linux_fs_start == 1)
        puma5_partitions2[0].name   = "filesystem";
    else
        puma5_partitions2[0].name   = "filesystem_reserved";
    puma5_partitions2[0].offset = puma5_partitions2[2].size;
    puma5_partitions2[0].size   = puma5_partitions[0].size;
    puma5_partitions2[0].mask_flags = 0;

    /*--------------------------------------------------------------------------------------*\
     * kernel image am anfang f�r den Parser
    \*--------------------------------------------------------------------------------------*/
    if (linux_fs_start == 1)
        puma5_partitions2[1].name   = "kernel";
    else
        puma5_partitions2[1].name   = "kernel_reserved";
    puma5_partitions2[1].offset = puma5_partitions2[2].size;
    puma5_partitions2[1].size   = puma5_partitions[1].size;
    puma5_partitions2[1].mask_flags = 0;

    if (linux_fs_start == 1)
        puma5_partitions2[3].name   = "extra";     /*--- Kernel und Filesystem zusammen ---*/
    else
        puma5_partitions2[3].name   = "extra_reserved";     /*--- Kernel und Filesystem zusammen ---*/
    puma5_partitions2[3].offset = puma5_partitions2[2].size;
    puma5_partitions2[3].size   = puma5_partitions[1].size;
    puma5_partitions2[3].mask_flags = 0;

    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
    DEBUG_MTD("special mtd_parser registered\n");
    puma5_init_platform_devices();
    register_mtd_user(&puma5_mtd_notifier);
    register_mtd_parser(&puma5_squashfs_parser);

    return 0;
}
subsys_initcall(puma5_mtd_init);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int __init mtdram_setup(char *p) {
    DEBUG_MTD("[mtdram_setup] str=\"%s\"\n", p);
    if(p) {
        DEBUG_MTD("mtdram1 %s\n" , p);
        puma5_flash_resource[2].start  = (unsigned int)simple_strtoul(p, NULL, 16);
        puma5_flash_resource[2].start  = puma5_flash_resource[2].start;
        puma5_flash_resource[2].flags  = IORESOURCE_MEM,
        p = strchr(p, ',');
        if(p) {
            p++;
            puma5_flash_resource[2].end  = (unsigned int)simple_strtoul(p, NULL, 16);
            puma5_flash_resource[2].end -= 1;
        } else {
            puma5_flash_resource[2].start = 0;
        }
        DEBUG_MTD("mtdram1 0x%08x - 0x%08x\n" , puma5_flash_resource[2].start, puma5_flash_resource[2].end );
        puma5_ram_partitions[0].name		 = "filesystem_ram";
        puma5_ram_partitions[0].offset	 = 0;
        puma5_ram_partitions[0].size		 = puma5_flash_resource[2].end - puma5_flash_resource[2].start + 1;
        puma5_ram_partitions[0].mask_flags = MTD_ROM;
        puma5_ram_partitions[1].name		 = "unused_ram";
        puma5_ram_partitions[1].offset	 = 0;
        puma5_ram_partitions[1].size		 = puma5_flash_resource[2].end - puma5_flash_resource[2].start + 1;
        puma5_ram_partitions[1].mask_flags = MTD_ROM;
        puma5_ram_partitions[2].name		 = "extra_ram";
        puma5_ram_partitions[2].offset	 = 0;
        puma5_ram_partitions[2].size		 = puma5_flash_resource[2].end - puma5_flash_resource[2].start + 1;
        puma5_ram_partitions[2].mask_flags = MTD_ROM;

        puma5_max_plattforms += 1;      /*--- die RAM-Partition zu den Plattformdevices hinzuf�gen ---*/
    }
    return 0;
}
__setup("mtdram1=", mtdram_setup);

#endif


