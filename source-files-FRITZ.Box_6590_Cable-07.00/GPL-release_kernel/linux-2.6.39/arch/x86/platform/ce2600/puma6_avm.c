#include <asm/mach_avm.h>
#include <asm/prom.h>
#include <asm/byteorder.h>

#include <linux/of.h>
#include <linux/proc_fs.h>
#include <linux/vmalloc.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/partitions.h>
#include <linux/sched.h>

#include "ce2600-gpio.h"

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
//#define DEBUG_WLAN_DECT_CONFIG
#if defined(DEBUG_WLAN_DECT_CONFIG)
#define DBG_WLAN_DECT(arg...) printk(KERN_ERR arg)
#else
#define DBG_WLAN_DECT(arg...) 
#endif

char *prom_getenv(char *name)
{
    const char *entry = NULL;
    struct device_node *chosen;

    chosen = of_find_node_by_name(NULL, "chosen");
    if(chosen){
        entry = of_get_property(chosen, name, NULL );
    }

    return (char *)entry;
}
EXPORT_SYMBOL(prom_getenv);

static loff_t wlan_dect_config[PUMA_MAX_CONFIG_ENTRIES];
extern struct mtd_info *puma6_urlader_mtd;

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
void set_wlan_dect_config_address(unsigned int value)
{
    int i = 0;

    DBG_WLAN_DECT("[%s] wlan_dect_config\n", __FUNCTION__);
    while (i < PUMA_MAX_CONFIG_ENTRIES) {
        if (wlan_dect_config[i] == 0) {
            DBG_WLAN_DECT("[%s] wlan_dect_config[%d] 0x%lx\n", __FUNCTION__, i, wlan_dect_config[i]);
            wlan_dect_config[i] = value;
            break;
        }

        ++i;
    }
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int init_wlan_dect_config(void)
{
    const char *entry = NULL;
    struct device_node *chosen;
    uint64_t cfg_offset;
    int entry_len, i;

    chosen = of_find_node_by_name(NULL, "chosen");
    if(!chosen){
        printk(KERN_WARNING "[%s] Device tree node \"chosen\" not found\n", __func__);
        return -1;
    }

    entry = of_get_property(chosen, "wlan_dect_configs", &entry_len);
    printk(KERN_ERR "[%s] entry: %p entry_len: 0x%x\n", __func__, entry, entry_len);

    if(!entry || entry_len == 0 || (entry_len % sizeof(uint64_t)) != 0){
        printk(KERN_WARNING "[%s] No property \"wlan_dect_configs\" found in device tree or property length invalid\n", __func__);
        return -1;
    }

    for(i = 0; i < entry_len; i += 8){
        printk(KERN_ERR "[%s] %d: 0x%02x%02x%02x%02x%02x%02x%02x%02x\n", __func__, i / 8, (unsigned char) entry[i], (unsigned char) entry[i + 1], (unsigned char) entry[i + 2], (unsigned char) entry[i + 3], (unsigned char) entry[i + 4], (unsigned char) entry[i + 5], (unsigned char) entry[i + 6], (unsigned char) entry[i + 7]);
    }

    i = 0;
    while(entry_len > 0 && i < PUMA_MAX_CONFIG_ENTRIES){
        cfg_offset = of_read_number((__be32 *)entry, sizeof(uint64_t) / sizeof(uint32_t));
        entry += sizeof(uint64_t);
        entry_len -= sizeof(uint64_t);

        if(cfg_offset > 0){
            wlan_dect_config[i] = (loff_t) cfg_offset;
            DBG_WLAN_DECT("[%s] Found config entry pointer: 0x%08lx\n", __func__, cfg_offset);
            ++i;
        }
    }

    printk(KERN_INFO "[%s] Found %d config entry pointers.\n", __func__, i);

    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int get_wlan_dect_config(enum wlan_dect_type Type, unsigned char *buffer, unsigned int len)
{
    int i, result;
    unsigned int readlen = 0;
    struct wlan_dect_config config, *tmp_cfg;
    loff_t offset;
    unsigned char tmpbuffer[sizeof(struct wlan_dect_config) + 1];
    unsigned char *tmpbuff_p;

    DBG_WLAN_DECT("[%s] Type %d buffer 0x%p len %d\n", __func__, Type, buffer, len);

    if(wlan_dect_config[0] == 0){
        result = init_wlan_dect_config();
        if(result != 0){
            printk(KERN_WARNING "[%s] Could not init WLAN DECT config pointers\n", __func__);
            return result;
        }
    }

    // word-align start of temp. buffer
    tmpbuff_p = &tmpbuffer[0];
    if((unsigned long) tmpbuff_p & 1ul){
        ++tmpbuff_p;
    }

    tmp_cfg = (struct wlan_dect_config *) tmpbuff_p;

    for(i = 0; i < PUMA_MAX_CONFIG_ENTRIES; i++){
        DBG_WLAN_DECT("[%s] wlan_dect_config[%d] 0x%lx\n", __func__, i, wlan_dect_config[i]);

        if(wlan_dect_config[i]){ /*--- Eintrag vorhanden und nicht leer ---*/
            offset = wlan_dect_config[i];
            puma6_urlader_mtd->read(puma6_urlader_mtd,
                                    offset,
                                    sizeof(struct wlan_dect_config),
                                    &readlen,
                                    tmpbuff_p);

            if(readlen != sizeof(struct wlan_dect_config)){
                DBG_WLAN_DECT("[%s] ERROR: read wlan_dect_config %d\n", __func__, readlen);
                return -1;
            }

            config.Version = tmp_cfg->Version;
            config.Type = tmp_cfg->Type;
            config.Len = be16_to_cpu(tmp_cfg->Len);

            DBG_WLAN_DECT("[%s] Version 0x%x Type %d Len 0x%x\n",
                          __func__,
                          config.Version,
                          config.Type,
                          config.Len);

            switch(config.Version){
            case 1:
            case 2:
                DBG_WLAN_DECT("[%s] Type %d Len 0x%x\n",
                              __func__,
                              config.Type,
                              config.Len);
                if(Type != config.Type){
                    break; /*--- nächster Konfigeintrag ---*/
                }
                if(!(len >= config.Len + sizeof(struct wlan_dect_config)))
                    return -2; /*--- buffer zu klein ---*/
                DBG_WLAN_DECT("[%s] read ", __func__);
                switch(config.Type){
                case WLAN:
                case WLAN2:
                    DBG_WLAN_DECT("WLAN\n");
                    break;
                case DECT:
                    DBG_WLAN_DECT("DECT\n");
                    break;
                case DOCSIS:
                    DBG_WLAN_DECT("DOCSIS\n");
                    break;
                case ZERTIFIKATE:
                    DBG_WLAN_DECT("ZERTIFIKATE\n");
                    break;
                default:
                    DBG_WLAN_DECT("[%s] unknown Version %x\n", __FUNCTION__, config.Version);
                    return -3;
                }
                result = puma6_urlader_mtd->read(puma6_urlader_mtd,
                                                 offset,
                                                 config.Len + sizeof(struct wlan_dect_config),
                                                 &readlen,
                                                 buffer);

                if(result < 0 || readlen != (config.Len + sizeof(struct wlan_dect_config))){
                    DBG_WLAN_DECT("ERROR: read Data\n");
                    return -5;
                }

                tmp_cfg = (struct wlan_dect_config *)buffer;
                tmp_cfg->Len = be16_to_cpu(tmp_cfg->Len);

                return 0;
            default:
                DBG_WLAN_DECT("[%s] unknown Version %x\n", __func__, config.Version);
                return -3;
            }
        }
    }

    return -1;
}
EXPORT_SYMBOL(get_wlan_dect_config);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
static int search_wlan_dect_config(enum wlan_dect_type Type, struct wlan_dect_config *config) {
    int i, result;
    unsigned int readlen = 0;
    struct wlan_dect_config *tmp_cfg;
    int offset;
    unsigned char tmpbuffer[2 * sizeof(struct wlan_dect_config) + 1];
    unsigned char *tmpbuff_p;

    if (!config) {
        printk( KERN_ERR "[%s] ERROR: no configbuffer\n", __FUNCTION__);
        return -1;
    }

    if(wlan_dect_config[0] == 0){
        result = init_wlan_dect_config();
        if(result != 0){
            printk(KERN_WARNING "[%s] Could not init WLAN DECT config pointers\n", __func__);
            return -1;
        }
    }

    // word-align start of temp. buffer
    tmpbuff_p = &tmpbuffer[0];
    if((unsigned long) tmpbuff_p & 1ul){
        ++tmpbuff_p;
    }

    tmp_cfg = (struct wlan_dect_config *) tmpbuff_p;

    for(i = 0; i < PUMA_MAX_CONFIG_ENTRIES; i++){
        if (wlan_dect_config[i]) {    /*--- Eintrag vorhanden und nicht leer ---*/
            offset = wlan_dect_config[i];
            puma6_urlader_mtd->read(puma6_urlader_mtd,
                                    offset,
                                    sizeof(struct wlan_dect_config),
                                    &readlen,
                                    tmpbuff_p);

            if(readlen != sizeof(struct wlan_dect_config)){
                DBG_WLAN_DECT("[%s] ERROR: read wlan_dect_config %d\n", __func__, readlen);
                return -2;
            }

            switch (tmp_cfg->Version) {
                case 1:
                case 2:
                    DBG_WLAN_DECT("[%s] Type %d Len 0x%x\n", __FUNCTION__, config->Type, config->Len);
                    if (Type == tmp_cfg->Type) {
                        memcpy(config, tmp_cfg, sizeof(struct wlan_dect_config));
                        return 1;
                    }
                    break;
                default:
                    printk(KERN_ERR "[%s] ERROR: unknown ConfigVersion 0x%x\n", __FUNCTION__, config->Version);
                    break;
            }
        }
    }

    /*--- nix gefunden ---*/
    memset(config, 0, sizeof(struct wlan_dect_config));
    return 0;
}

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int test_wlan_dect_config(char *buffer, size_t *bufferlen) {

    struct wlan_dect_config config;
    enum wlan_dect_type count = WLAN;
    int tmp = 0, len, error = 0;

    len = *bufferlen;
    *bufferlen = 0;
    buffer[0] = 0;  /*--- damit strcat auch funktioniert ---*/

    while (count < MAX_TYPE) {
        if (search_wlan_dect_config(count, &config)) {
            switch (config.Version) {
                case 1:
                case 2:
                    switch (config.Type) {
                        case WLAN:
                            strcat(buffer, "WLAN\n");
                            tmp = strlen("WLAN\n");
                            break;
                        case WLAN2:
                            strcat(buffer, "WLAN2\n");
                            tmp = strlen("WLAN2\n");
                            break;
                        case DECT:
                            strcat(buffer, "DECT\n");
                            tmp = strlen("DECT\n");
                            break;
                        case DOCSIS:
                            strcat(buffer, "DOCSIS\n");
                            tmp = strlen("DOCSIS\n");
                            break;
                        case ZERTIFIKATE:
                            strcat(buffer, "ZERTIFIKATE\n");
                            tmp = strlen("ZERTIFIKATE\n");
                            break;
                        default:
                            printk(KERN_ERR "[%s] ERROR: unknown ConfigVersion 0x%x\n", __FUNCTION__, config.Version);
                            error = -1;
                    }
                    break;
                default:
                    printk(KERN_ERR "[%s] ERROR: unknown ConfigVersion 0x%x\n", __FUNCTION__, config.Version);
                    error = -1;
            }
            if (len > tmp) {
                len -= tmp;
                *bufferlen += tmp;
            } else {
                DBG_WLAN_DECT( KERN_ERR "[%s] ERROR: Buffer\n", __FUNCTION__);
                error = -1;
            }
        }
        count++;
    }

    return error;

}
EXPORT_SYMBOL(test_wlan_dect_config);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
#include <linux/fs.h>
#include <linux/file.h>
#include <asm/io.h>
#include <asm/fcntl.h>
#include <asm/errno.h>
#include <asm/ioctl.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/mman.h>

int copy_wlan_dect_config2user(char *buffer, size_t bufferlen) {

    struct wlan_dect_config config;
    char *ConfigStrings[5] = { "WLAN", "DECT", "WLAN2", "ZERTIFIKATE", "DOCSIS" };
    enum wlan_dect_type Type;
    char *p, *vbuffer, *map_buffer;
    struct file *fp;
    int    configlen, written;

    if (!bufferlen)
        return -1;

    if (buffer[bufferlen-1] == '\n') {      /*--- \n entfernen ---*/
        buffer[bufferlen-1] = 0;
        bufferlen--;
    }

    for (Type = WLAN; Type < MAX_TYPE; Type++) {
        p = strstr(buffer, ConfigStrings[Type]);
        if (p) {
            if ((Type == WLAN) && (buffer[4] == '2'))   /*--- WLAN & WLAN2 unterscheiden ---*/
                continue;
            p += strlen(ConfigStrings[Type]);
            break;
        }
    }

    if (!p) {
        printk(KERN_ERR "ERROR: Type unknown\n");
        return -1;
    }

    while (*p && (*p == ' ') && (p < &buffer[bufferlen]))   /*--- die spaces im Pfadnamen löschen ---*/
       p++;

    if (!search_wlan_dect_config(Type, &config)) {
        printk(KERN_ERR "ERROR: no Config found\n");
        return -1;  /*--- keine Config gefunden ---*/
    }

    configlen = config.Len + sizeof(struct wlan_dect_config);     /*--- wir müssen den Header mitlesen ---*/

    fp = filp_open(p, O_CREAT, FMODE_READ|FMODE_WRITE);  /*--- open read/write ---*/
    if(IS_ERR(fp)) {
        printk("ERROR: Could not open file %s\n", p);
        return -1;
    }

    map_buffer = (unsigned char *)do_mmap(0, 0, configlen, PROT_READ|PROT_WRITE, MAP_SHARED, 0);
    if (IS_ERR(buffer)) {
        printk("ERROR: no mem 0x%p\n", map_buffer);
        return -1;
    }

    vbuffer = (char *)vmalloc(configlen);       /*--- wir brauchen einen Buffer zum umkopieren ---*/
    if (!vbuffer) {
        printk("ERROR: no mem\n");
        return -1;
    }
    if (!get_wlan_dect_config(Type, vbuffer, configlen)) {
        memcpy(map_buffer, &vbuffer[sizeof(struct wlan_dect_config)], config.Len);   /*--- umkopieren & den Header verwerfen ---*/
        written = fp->f_op->write(fp, map_buffer, config.Len, &fp->f_pos);      /*--- die Datei schreiben ---*/

        do_munmap(current->mm, (unsigned long)map_buffer, configlen);    /*--- den buffer wieder frei geben ---*/
        vfree(vbuffer);
        if (written != config.Len) {
            printk("ERROR: write Config\n");
            return -1;
        }
    } else {
        do_munmap(current->mm, (unsigned long)map_buffer, configlen);    /*--- den buffer wieder frei geben ---*/
        vfree(vbuffer);
        printk("ERROR: read Config\n");
        return -1;
    }

    return 0;

}
EXPORT_SYMBOL(copy_wlan_dect_config2user);

/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int x86_gpio_ctrl(unsigned int gpio_pin, enum _hw_gpio_function pin_mode, enum _hw_gpio_direction pin_dir) {
    struct gpio_chip *chip;
    /*--- printk(KERN_ERR"%s(%d) %s %s\n", __func__, gpio_pin, pin_mode == FUNCTIONAL_PIN ? "func" : "gpio", pin_dir == GPIO_OUTPUT_PIN ? "out" : "in"); ---*/
    chip = intelce_get_gpio_chip(&gpio_pin);
    if(chip == NULL) {
        printk(KERN_ERR"%s(%d) failed\n", __func__, gpio_pin);
        return -1;
    }
    if(pin_dir == GPIO_OUTPUT_PIN) {
        ce2600_gpio_direction_output(chip, gpio_pin, ce2600_gpio_input(chip, gpio_pin));
    } else {
        ce2600_gpio_direction_input(chip, gpio_pin);
    }
    return ce2600_set_multi_function(chip, gpio_pin, pin_mode == FUNCTIONAL_PIN ? 1 : 0);
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int x86_gpio_out_bit(unsigned int gpio_pin, int value) {
    struct gpio_chip *chip;
    /*--- printk(KERN_ERR"%s(%d) set=%x\n", __func__, gpio_pin, value); ---*/
    chip = intelce_get_gpio_chip(&gpio_pin);
    if(chip == NULL) {
        printk(KERN_ERR"%s(%d) failed\n", __func__, gpio_pin);
        return -1;
    }
    ce2600_gpio_set(chip, gpio_pin, value);
    return 0;
}
/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int x86_gpio_in_bit(unsigned int gpio_pin) {
    struct gpio_chip *chip = intelce_get_gpio_chip(&gpio_pin);
    if(chip == NULL) {
        printk(KERN_ERR"%s(%d) failed\n", __func__, gpio_pin);
        return -1;
    }
    return ce2600_gpio_input(chip, gpio_pin);
}
/*--------------------------------------------------------------------------------*\
 * liefere regbase fuer in
 * use bitbang-function instead
\*--------------------------------------------------------------------------------*/
void __deprecated *x86_gpioin_regbase(unsigned int gpio_pin) {
    struct gpio_chip *chip = intelce_get_gpio_chip(&gpio_pin);
	struct intelce_gpio_chip *c;
    if(chip == NULL) {
        return NULL;
    }
	c = to_intelce_gpio_chip(chip);
    if(c == NULL) {
        return NULL;
    }
    return c->reg_base + CE2600_PUB_GPIO_INPUT;
}
EXPORT_SYMBOL(x86_gpioin_regbase);
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
struct _gpio_bitbang_ctrl {
    void __iomem *read_base;
    void __iomem *setclr_base[2];
    unsigned int bit_pos;
};
/**--------------------------------------------------------------------------------**\
 * only use x86_gpio_bitbang_in_bit() or x86_gpio_bitbang_out_bit()
 * you know what you do !
\**--------------------------------------------------------------------------------**/
void *x86_gpio_get_bitbang_handle(unsigned int gpio_pin) {
    struct _gpio_bitbang_ctrl *pfa_gpio;
    struct gpio_chip *chip = intelce_get_gpio_chip(&gpio_pin);
	struct intelce_gpio_chip *c;
    if(chip == NULL) {
        return NULL;
    }
	c = to_intelce_gpio_chip(chip);
    if(c == NULL) {
        return NULL;
    }
    pfa_gpio = kzalloc(sizeof(struct _gpio_bitbang_ctrl), GFP_ATOMIC);
    if(pfa_gpio == NULL) {
		return NULL;
    }
	pfa_gpio->read_base      = c->reg_base  + CE2600_PUB_GPIO_INPUT;
	pfa_gpio->setclr_base[0] = c->high_base + CE2600_PUB_GPIO_CLEAR;
	pfa_gpio->setclr_base[1] = c->high_base + CE2600_PUB_GPIO_SET;
	pfa_gpio->bit_pos = gpio_pin;
    return pfa_gpio;
}
EXPORT_SYMBOL(x86_gpio_get_bitbang_handle);
/**--------------------------------------------------------------------------------**\
 * free handle
\**--------------------------------------------------------------------------------**/
void x86_gpio_put_bitbang_handle(void *handle) {
    if(handle) {
        kfree(handle);
    }
}
EXPORT_SYMBOL(x86_gpio_put_bitbang_handle);
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
int x86_gpio_bitbang_in_bit(void *handle) {
    struct _gpio_bitbang_ctrl *pfa_gpio = (struct _gpio_bitbang_ctrl *)handle;
	return intelce_gpio_mmio_read32(pfa_gpio->read_base) & (1 << pfa_gpio->bit_pos) ? 1 : 0;
}
EXPORT_SYMBOL(x86_gpio_bitbang_in_bit);
/**--------------------------------------------------------------------------------**\
\**--------------------------------------------------------------------------------**/
void x86_gpio_bitbang_out_bit(void *handle, unsigned int set) {
    struct _gpio_bitbang_ctrl *pfa_gpio = (struct _gpio_bitbang_ctrl *)handle;
	intelce_gpio_mmio_write32(1 << pfa_gpio->bit_pos, pfa_gpio->setclr_base[set ? 1 : 0]);
}
EXPORT_SYMBOL(x86_gpio_bitbang_out_bit);
#ifdef CONFIG_PROC_FS
/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int proc_avm_gpio_read(char *page, char **start, off_t off, int count, int *eof, void *data) {
    return 0;
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int proc_avm_gpio_write(struct file *filp, const char __user *write_buffer, unsigned long write_length, void *data) {
    char Buffer[128], *p;
    int ret = 0;
    unsigned int gpio, val;
    static struct _cmd_gpio {
        char *cmd; 
        unsigned int len;
        enum { dir, set, func, list } cmd_tag;
#define SET_CMD(param) { cmd: #param, len: sizeof(#param) -1, cmd_tag: param }
    } *pcmd_gpio, cmd_gpio[] = {
        SET_CMD(dir),
        SET_CMD(set),
        SET_CMD(func),
        SET_CMD(list),
        { cmd: NULL }
    };
    if(write_length >= sizeof(Buffer)) {
        write_length = sizeof(Buffer) - 1;
    }
    if(copy_from_user(Buffer, write_buffer, write_length)) {
        return -EFAULT;
    }
    Buffer[write_length] = '\0';
    pcmd_gpio= cmd_gpio;
    while(pcmd_gpio->cmd) {
        if((p = strstr(Buffer, pcmd_gpio->cmd))) {
            p += pcmd_gpio->len;
            sscanf(p, "%d %d", &gpio, &val);
            switch(pcmd_gpio->cmd_tag) {
                case dir: 
                    printk(KERN_DEBUG "gpio_pin=%d, pin_direction=%x(%s)\n",gpio, val, val == GPIO_OUTPUT_PIN ? "out" : "in");
                    ret = x86_gpio_ctrl(gpio, GPIO_PIN, val);
                    break;
                case set: 
                    printk(KERN_DEBUG "gpio_pin=%d, set=%x\n",gpio, val);
                    ret = x86_gpio_out_bit(gpio, val);
                    break;
                case func: {
                    int gpio_per_bank = gpio;
                    struct gpio_chip *chip = intelce_get_gpio_chip(&gpio_per_bank);
                    if(chip == NULL) {
                        ret =  -1;
                        break;
                    }
                    printk(KERN_DEBUG "gpio_pin=%d, func=%x\n",gpio, val);
                    ret = ce2600_set_multi_function(chip, gpio_per_bank, val);
                    break;
                    }
                case list: 
                    break;
            }
            if(ret) {
                printk(KERN_ERR"failed ret=%d\n", ret);
            }
        }
        pcmd_gpio++;
    }
    return write_length;
}

/*--------------------------------------------------------------------------------*\
\*--------------------------------------------------------------------------------*/
static int __init proc_avm_gpio_init(void) {
	struct proc_dir_entry *res;

	res = create_proc_entry("avm/gpio", S_IWUSR | S_IRUGO, NULL);
	if (!res)
		return -ENOMEM;

	res->read_proc = proc_avm_gpio_read;
	res->write_proc = proc_avm_gpio_write;

	return 0;
}
__initcall(proc_avm_gpio_init);
#endif /* CONFIG_PROC_FS */

EXPORT_SYMBOL(x86_gpio_ctrl);
EXPORT_SYMBOL(x86_gpio_out_bit);
EXPORT_SYMBOL(x86_gpio_in_bit);

extern ssize_t intelce_get_arm_memsize(void);
struct resource *puma6_get_arm_ram(void)
{
    static struct resource *arm_ram = NULL;
    static int init_done = 0;
    ssize_t memsize;

    if(init_done == 0){
        memsize = intelce_get_arm_memsize();
        if(memsize > 0){
//            pr_info("[%s] memsize: 0x%08x\n", __func__, memsize);
            arm_ram = request_mem_region((resource_size_t) 0x08000000, memsize, "ARM-RAM");
            init_done = 1;
        }
    }

    return arm_ram;
}
EXPORT_SYMBOL(puma6_get_arm_ram);
