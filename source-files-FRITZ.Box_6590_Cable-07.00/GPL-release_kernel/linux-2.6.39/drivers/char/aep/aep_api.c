/*
* File Name: aep_api.c
*/
/*

  This file is provided under a dual BSD/GPLv2 license.  When using or
  redistributing this file, you may do so under either license.

  GPL LICENSE SUMMARY

  Copyright(c) 2007-2012 Intel Corporation. All rights reserved.

  This program is free software; you can redistribute it and/or modify
  it under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
  The full GNU General Public License is included in this distribution
  in the file called LICENSE.GPL.

  Contact Information:

  */
#include <linux/aep.h>
#include "aep_core.h"
#include "aep_host_ipc.h"


/**
checks for API entry prerequisites for APIs.
returns a non-success value if they are not all matched.
*/
static aep_result_t aep_api_enter(const char *fn_name)
{
	aep_result_t result = AEP_SUCCESS;
	if (!aep_is_active()) {
		LOG_ERROR("%s: Driver not Initalized\n", fn_name);
		result = AEP_ERROR_ALREADY_INITIALIZED;
	} else {
		if (aep_hw_device_suspended()) {
			LOG_ERROR("%s: Driver Suspended\n", fn_name);
			result = AEP_ERROR_NO_RESOURCES;
		}
	}

	return result;
}

/**
Checks for valid state  for normal API functions
result will be AEP_SUCCESS if the code immediately after this macro
is run.
calls lock_fn to acquire locks
*/
#define AEP_API_ENTER(result) do {\
	AEP_FUNC_ENTER();                             \
	*result = aep_api_enter(__func__);   \
	if (*result != AEP_SUCCESS)                 \
		goto exit;                       \
	} while (0);

/**
Marks then end of the code to run if AEP_API_ENTER was run
successfully.

*/
#define AEP_API_EXIT() do {\
 exit:                 \
	AEP_FUNC_EXIT();		\
	} while (0);

/*
 * Read from GPIO register .
 *
 * aep_gpio_read(unsigned int reg_offset, unsigned int* value)
 *
 * Description:
 *  This API is used to read GPIO register when the AEP is active.
 *
 * Precondition:
 *  AEP must be active
 *
 * Parameters:
 *  reg_offset   - GPIO  register offset to read from.
 *  value        - [output], the read value.
 *
 * Return:
 *  0 on Success, <0 on error.
 */

aep_result_t aep_gpio_read(uint32_t gpio_address_offset, uint32_t *reg_value)
{
	ipl_t gpio_ipl;
	aep_ipc_sizes_t io_sizes;
	uint32_t output;
	aep_result_t rc = AEP_SUCCESS;

	AEP_API_ENTER(&rc);

	VERIFY(NULL != reg_value, exit, rc, AEP_ERROR_OPERATION_FAILED);

	memset(&gpio_ipl, 0, sizeof(gpio_ipl));

	gpio_ipl.gpio_read_write.gpio_addr_offset = gpio_address_offset;
	gpio_ipl.gpio_read_write.transaction_type = GPIO_READ;

	io_sizes.ipl_size = sizeof(ipl_gpio_read_write_t);
	io_sizes.opl_size = sizeof(uint32_t);

	rc = aep_host_send_ipc(IPC_AEP_READ_WRITE, io_sizes,
						&gpio_ipl, (opl_t *)&output);


	AEP_API_EXIT();

	return rc;

}
EXPORT_SYMBOL(aep_gpio_read);


/*
 * Write to GPIO register.
 *
 * int aep_gpio_write(unsigned int reg_offset,
 *                     unsigned int bit num, unsigned int value)
 *
 * Description:
 *  This API is used to write to a GPIO register when the AEP is active.
 *
 * Precondition:
 *  AEP must be active
 *
 * Parameters:
 *  reg_offset   - GPIO  register offset to read from.
 *  bit num        - The mask or bit num where value needs to be updated
 *  config         - Value to be written
 *
 * Return:
 *  0 on Success, <0 on error.
 */
aep_result_t aep_gpio_write(uint32_t gpio_address_offset,
					uint32_t bit_num, uint32_t gpio_config)

{
	ipl_t gpio_ipl;
	aep_ipc_sizes_t io_sizes;
	aep_result_t rc = AEP_SUCCESS;

	AEP_API_ENTER(&rc);

	memset(&gpio_ipl, 0, sizeof(gpio_ipl));

	gpio_ipl.gpio_read_write.gpio_addr_offset = gpio_address_offset;
	gpio_ipl.gpio_read_write.transaction_type = GPIO_WRITE;
	gpio_ipl.gpio_read_write.write_mask = (1<<bit_num);

	if (0 == gpio_config)
		gpio_ipl.gpio_read_write.write_data = 0;
	else
		gpio_ipl.gpio_read_write.write_data = (1<<bit_num);

	io_sizes.ipl_size = sizeof(ipl_gpio_read_write_t);

	rc = aep_host_send_ipc(IPC_AEP_READ_WRITE, io_sizes, &gpio_ipl, NULL);

	AEP_API_EXIT();

	return rc;

}
EXPORT_SYMBOL(aep_gpio_write);

/*
 * Write to GPIO register.
 *
 * int aep_gpio_write_lockfree(unsigned int reg_offset,
 *                             unsigned int bit num, unsigned int value)
 *
 * Description:
 *  This API is used to write to a GPIO register when the AEP is active.
 *  It writes to GPIO register using workqueue, thus it will not block the
 *  calling API to wait for a thread to release the lock for using AEP IPC.
 *
 * Precondition:
 *  AEP must be active
 *
 * Parameters:
 *  reg_offset   - GPIO  register offset to read from.
 *  bit num        - The mask or bit num where value needs to be updated
 *  config         - Value to be written
 *
 * Return:
 *  0 on Success, <0 on error.
 */
aep_result_t aep_gpio_write_no_lock(uint32_t gpio_address_offset,
					uint32_t bit_num, uint32_t gpio_config)
{

	ipl_t *gpio_ipl;
	aep_result_t rc = AEP_SUCCESS;

	AEP_API_ENTER(&rc);

	gpio_ipl = kmalloc(sizeof(ipl_t), GFP_ATOMIC);

	if (NULL == gpio_ipl) {
		printk(KERN_ERR "\n Null pointer: Memory allocation failed\n");
		rc = AEP_OUT_OF_MEMORY;
		goto exit;
	}

	memset(gpio_ipl, 0, sizeof(gpio_ipl));

	gpio_ipl->gpio_read_write.gpio_addr_offset = gpio_address_offset;
	gpio_ipl->gpio_read_write.transaction_type = GPIO_WRITE;
	gpio_ipl->gpio_read_write.write_mask = (1<<bit_num);

	if (0 == gpio_config)
		gpio_ipl->gpio_read_write.write_data = 0;
	else
		gpio_ipl->gpio_read_write.write_data = (1<<bit_num);

	rc = aep_host_send_ipc_wq(IPC_AEP_READ_WRITE, gpio_ipl, NULL);

	AEP_API_EXIT();

	return rc;

}
EXPORT_SYMBOL(aep_gpio_write_no_lock);
/*
 * Get AEP F/W Version.
 *
 * int aep_get_fw(void)
 *
 * Description:
 *  This API is used to read the AEP F/W version
 *
 * Precondition:
 *  This API can be only if the AEP is active.
 *
 * Parameters:
 *  None.
 *
 * Return:
 *  AEP F/W version
 */

aep_result_t aep_get_fw(uint32_t *fw_ver)
{

	aep_result_t rc = AEP_SUCCESS;
	uint32_t fw_version;
	aep_ipc_sizes_t io_sizes;

	AEP_API_ENTER(&rc);

	io_sizes.ipl_size = 0;
	io_sizes.opl_size = sizeof(fw_version);

	rc = aep_host_send_ipc(IPC_AEP_FW_VER, io_sizes,
					NULL, (opl_t *) &fw_version);	

	AEP_API_EXIT();
	return rc;

}
EXPORT_SYMBOL(aep_get_fw);
