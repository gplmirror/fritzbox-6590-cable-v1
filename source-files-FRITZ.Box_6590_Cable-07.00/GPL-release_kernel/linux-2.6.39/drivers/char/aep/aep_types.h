/*
* File Name: aep_host_ipc.h
*/

/*
  This file is provided under a dual BSD/GPLv2 license.  When using or
  redistributing this file, you may do so under either license.

  GPL LICENSE SUMMARY

  Copyright(c) 2007-2012 Intel Corporation. All rights reserved.

  This program is free software; you can redistribute it and/or modify
  it under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
  The full GNU General Public License is included in this distribution
  in the file called LICENSE.GPL.

  Contact Information:
  Intel Corporation
  2200 Mission College Blvd.
  Santa Clara, CA  97052

*/
#ifndef AEP_TYPES_H
#define AEP_TYPES_H

/* Input payload*/

#define AEP_INPUT_PAYLOAD_BYTE_SIZE 64

typedef struct {
	uint32_t data[16];
} ipl_suspend_t;

typedef enum {
	GPIO_WRITE = 0,
	GPIO_READ  = 1
} aep_gpio_transaction_t;

typedef struct {
	uint32_t gpio_addr_offset;
	uint32_t transaction_type;
	uint32_t write_data;
	uint32_t write_mask;
} ipl_gpio_read_write_t;

/* generic input payload structure */
typedef union {
	uint8_t  data[AEP_INPUT_PAYLOAD_BYTE_SIZE];
	ipl_gpio_read_write_t  gpio_read_write;
	ipl_suspend_t  suspend_ipl;
} ipl_t;


typedef struct {
	uint32_t  data[16];
} opl_t;

typedef struct {
	/*Explicit byte size of input payload for AEP HW*/
	uint16_t        ipl_size;
	/*Explicit byte size of output payload returned from AEP HW*/
	uint16_t        opl_size;
} aep_ipc_sizes_t;

/** Context for Host IPC */
typedef struct {
	/** Device Handle for IPC Communication */
	void __iomem *dev_base;
	/*semaphore is for making IPC request to AEP*/
	struct semaphore          sync_request_sema;
	struct semaphore          sync_reponse_sema;
	struct semaphore          sync_msg_sema;
	/* semaphore for sending response to AEP */
	struct timer_list timer;	/* Timer for timeouts */
	int ipc_req;
	uint32_t error;
	struct tasklet_struct *schedule_tasklet;
	void			*done_data;	/* completion data */

} host_ipc_handler;

/* IPC COMMANDS*/

typedef enum {
	IPC_AEP_FW_VER              = 0x4,
	IPC_AEP_READ_WRITE          = 0x2,
} aep_fw_cmd_t;

/* AEP-To Host IPC COMMANDS */
typedef enum {
	AEP_FW_GPIO_CMD_DONE = 0x80000002,
	AEP_FW_NP_REQUEST = 0x5
} aep_host_cmd_t;
#endif
