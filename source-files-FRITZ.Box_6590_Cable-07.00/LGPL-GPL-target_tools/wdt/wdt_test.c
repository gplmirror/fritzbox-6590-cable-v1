#include <stdio.h>
#include <stdlib.h>
#include <wdt.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>



/*------------------------------------------------------------------------------------------*\
\*------------------------------------------------------------------------------------------*/
int main(int argc, char *argv[]) {
    int handle = 0;
    int max_fd;
    int i;
    int alive = 0;
    int zuende = 0;
    struct termios t;

    fd_set readfds;
    fd_set writefds;
    fd_set exceptfds;
    struct timeval timeout;

    for(i = 1 ; i < argc ; i++) {
        if(argv[i][0] == '-') {
            switch(argv[i][1]) {
                case '-':
                    i = argc;
                    break;
                case 'h':
                    printf("%s: -h (help) -a (alive)\n", argv[0]);
                    return 0;
                case 'a':
                    alive = 1;
                    break;
            }
        }
    }


    max_fd = 0; /*--- stdin ---*/

    FD_ZERO(&readfds);
    FD_ZERO(&writefds);
    FD_ZERO(&exceptfds);

    timeout.tv_sec  = 1000;
    timeout.tv_usec = 1000;

    tcgetattr(0, &t);
    t.c_lflag &= ~ICANON;
    tcsetattr(0, TCSANOW, &t);
    setbuf(stdin, NULL);

    FD_SET(0, &readfds);

    printf("[%s]:", argv[0]);

    /*--------------------------------------------------------------------------------------*\
    \*--------------------------------------------------------------------------------------*/
    for( ; zuende == 0; ) {
        int ret;
        FD_SET(0, &readfds);
        max_fd = 0;
        if(handle) {
            FD_SET(handle, &readfds);
            max_fd = handle;
        }
        /*----------------------------------------------------------------------------------*\
        \*----------------------------------------------------------------------------------*/
        i = select(max_fd + 1, &readfds, &writefds, &exceptfds, &timeout);

        /*----------------------------------------------------------------------------------*\
        \*----------------------------------------------------------------------------------*/
        if(FD_ISSET(0, &readfds)) {
            char ch;
            ret = read(0, &ch, sizeof(ch));
            if(ret <= 0)
                continue;
            printf(" %c\n", ch);
            switch(ch) {
                case 'h':
                    printf("help: [q]uit [r]egister [R]elease [a]nnounce [t]rigger\n");
                    break;
                case 'r':
                    handle = watchdog_register(argv[0]);
                    FD_SET(handle, &readfds);
                    max_fd = handle;
                    break;
                case 'R':
                    if(handle) {
                        watchdog_release(handle);
                        FD_CLR(handle, &readfds);
                        handle = 0;
                        max_fd = 0; /*--- stdin ---*/
                    }
                    break;
                case 'a':
                    if(handle) {
                        watchdog_announce(argv[0], 20);
                    }
                    break;
                case 't':
                    if(handle) {
                        watchdog_trigger(handle);
                    }
                    break;
                case 'q':
                    zuende = 1;
                    break;
            }
            printf("[%s]:", argv[0]);
        }

        /*----------------------------------------------------------------------------------*\
        \*----------------------------------------------------------------------------------*/
        if(handle && FD_ISSET(handle, &readfds)) {
            char Buffer[80];
            ret = read(handle, Buffer, sizeof(Buffer));
            printf("WDT(%u): read '%s'\n", ret, Buffer);

            if(alive) {
                char Trigger[] = "trigger";
                ret = write(handle, Trigger, sizeof(Trigger));
                printf("WDT(%u): write %s\n", ret, Trigger);
            }
        }
    }
    if(handle)
        watchdog_release(handle);
    return 0;
}
