/*
 *  arch/arm/include/asm/timex.h
 *
 *  Copyright (C) 1997,1998 Russell King
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 *  Architecture Specific TIME specifications
 */
#ifndef _ASMARM_TIMEX_H
#define _ASMARM_TIMEX_H

#include <mach/timex.h>

typedef unsigned long cycles_t;

#if defined(CONFIG_ARCH_PUMA5) || defined(CONFIG_MACH_PUMA5) || defined(CONFIG_MACH_PUMA6)

extern cycles_t get_puma_cpu_cycles(void);

static inline cycles_t get_cycles(void) {
	return get_puma_cpu_cycles();
}
#else

static inline cycles_t get_cycles (void)
{
	return 0;
}
#endif

#endif
