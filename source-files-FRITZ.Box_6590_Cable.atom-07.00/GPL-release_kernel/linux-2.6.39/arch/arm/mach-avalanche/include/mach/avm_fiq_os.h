  /*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2006 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/

#ifndef _AVM_FIQ_OS_H_
#define _AVM_FIQ_OS_H_


#if defined(CONFIG_AVM_FIQ_PUMA6)

#include <asm/mach_avm.h>

#include <linux/spinlock.h>
#include <mach/avm_fiq.h>
#define firq_is_linux_context()   (!is_fiq_context())
#define __arch_spin_lock(lock)    arch_spin_lock(lock)

/*--------------------------------------------------------------------------------*\
 * Speziallock: ist auch aus FASTIRQ aufrufbar
 * - schuetzt for IRQ und FIRQ
\*--------------------------------------------------------------------------------*/
#define firq_spin_lock_irqsave(lock, flags) 		\
do {							\
    flags = avm_arch_local_fiq_and_iq_save();           \
    if(!firq_is_linux_context()) {                      \
        __arch_spin_lock(&((lock)->rlock.raw_lock));    \
    } else {                                            \
        spin_lock(lock);                                \
    }                                                   \
} while (0)

/*--------------------------------------------------------------------------------*\
 * Speziallock: ist auch aus FASTIRQ aufrufbar
 * - restored IRQ und FIRQ
\*--------------------------------------------------------------------------------*/
static inline void firq_spin_unlock_irqrestore(spinlock_t *lock, unsigned long flags) {
    if(!firq_is_linux_context()) {
        arch_spin_unlock(&lock->rlock.raw_lock);
    } else {
        spin_unlock(lock);
    }
    avm_arch_local_fiq_restore(flags);
}

#define __BUILD_AVM_CONTEXT_FUNC(func) firq_##func

#else

#define __BUILD_AVM_CONTEXT_FUNC(func) func

#endif


#endif

