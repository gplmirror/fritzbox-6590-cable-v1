  /*------------------------------------------------------------------------------------------*\
 *   
 *   Copyright (C) 2016 AVM GmbH <fritzbox_info@avm.de>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
\*------------------------------------------------------------------------------------------*/

#ifndef _AVM_GIC_H_
#define _AVM_GIC_H_


#define OFFSET_ICDISER 0x28
#define OFFSET_ICDICER 0x2C
#define OFFSET_ICDISPR 0x20
#define OFFSET_ICDICPR 0x24
#define OFFSET_ICDIPTR 0x800
#define OFFSET_ICDIPR  0x400
#define OFFSET_ICDICR  0xD80
#define OFFSET_ICDISR  0xD00

#define OFFSET_ICCIAR  0x900
#define OFFSET_ICCEOIR 0x24
#define OFFSET_ICCHPIR 0x80

#define AVM_IRQ_MESSAGING_START 0xAC
#define AVM_IRQ_PROFILING_START (AVM_IRQ_MESSAGING_START + NR_CPUS)
#define AVM_IRQ_MONITOR_START   (AVM_IRQ_PROFILING_START + NR_CPUS)


#endif // #ifndef _AVM_GIC_H_
