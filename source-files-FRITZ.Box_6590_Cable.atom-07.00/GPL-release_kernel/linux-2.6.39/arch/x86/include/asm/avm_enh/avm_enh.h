#ifndef __avm_enh_h__
#define __avm_enh_h__

/**
 * vmalloc.c
 */
unsigned long get_vmap_area(unsigned long addr, unsigned long *caller, unsigned long *size, unsigned long *vmflags);
unsigned int get_used_vmalloc_mem(void);

/**
 * slab.c
 */
unsigned long get_kmemalloc_area(unsigned long addr, unsigned long *caller, const char **cache_name, 
								 unsigned long *size, int *freed);

/**
 * bthelper.c
 */
char *arch_print_memory_classifier(char *txt, unsigned int txtlen, unsigned long addr, int include_addr_prefix);
#define print_memory_classifier arch_print_memory_classifier

void arch_show_register_memoryclassifier(const struct pt_regs *pregs);
#define show_register_memoryclassifier arch_show_register_memoryclassifier
void arch_show_stacktrace_memoryclassifier(const struct pt_regs *pregs);
#define show_stacktrace_memoryclassifier arch_show_stacktrace_memoryclassifier
#endif
/* vim: set ts=8 sw=8 noet cino=>8\:0l1(0: */
