/*
 * x86 specific code for irq_work
 *
 * Copyright (C) 2010 Red Hat, Inc., Peter Zijlstra <pzijlstr@redhat.com>
 */

#include <linux/kernel.h>
#include <linux/irq_work.h>
#include <linux/hardirq.h>
#include <asm/apic.h>
#if defined(CONFIG_AVM_POWER)
#include <linux/avm_power.h>
#endif/*--- #if defined(CONFIG_AVM_POWER)  ---*/
#if defined(CONFIG_AVM_SIMPLE_PROFILING) 
#include <linux/avm_profile.h>
#endif/*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING)  ---*/

void smp_irq_work_interrupt(struct pt_regs *regs)
{
	irq_enter();
	ack_APIC_irq();
#if defined(CONFIG_AVM_POWER)
    avm_cpu_wait_end();  /*--- auch wenn es wait_irqoff gibt: trotzdem aufrufen, um system-load-Ausgabe zu triggern  ---*/ 
#endif/*--- #if defined(CONFIG_AVM_POWER)  ---*/
#if defined(CONFIG_AVM_SIMPLE_PROFILING) 
#if defined(avm_simple_profiling_enter_irq_context)
    avm_simple_profiling_enter_irq_context((unsigned int)instruction_pointer(regs), regs->bp);
#else/*--- #if defined(avm_simple_profiling_enter_irq_context) ---*/
    avm_simple_profiling_enter_irqcontext((unsigned int)instruction_pointer(regs));
#endif/*--- #else ---*//*--- #if defined(avm_simple_profiling_enter_irq_context) ---*/
    avm_simple_profiling_log(avm_profile_data_type_hw_irq_begin, (unsigned int)smp_irq_work_interrupt, 0x103);
#endif/*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING)  ---*/
	inc_irq_stat(apic_irq_work_irqs);
	irq_work_run();
#if defined(CONFIG_AVM_SIMPLE_PROFILING) 
    avm_simple_profiling_log(avm_profile_data_type_hw_irq_end, (unsigned int)smp_irq_work_interrupt, 0x103);
#endif/*--- #if defined(CONFIG_AVM_SIMPLE_PROFILING)  ---*/
	irq_exit();
}

void arch_irq_work_raise(void)
{
#ifdef CONFIG_X86_LOCAL_APIC
	if (!cpu_has_apic)
		return;

	apic->send_IPI_self(IRQ_WORK_VECTOR);
	apic_wait_icr_idle();
#endif
}
