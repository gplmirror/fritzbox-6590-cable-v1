/*
* File Name: aep.h
*/

/*
  This file is provided under a dual BSD/GPLv2 license.  When using or
  redistributing this file, you may do so under either license.

  GPL LICENSE SUMMARY

  Copyright(c) 2007-2014 Intel Corporation. All rights reserved.

  This program is free software; you can redistribute it and/or modify
  it under the terms of version 2 of the GNU General Public License as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful, but
  WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St - Fifth Floor, Boston, MA 02110-1301 USA.
  The full GNU General Public License is included in this distribution
  in the file called LICENSE.GPL.

  Contact Information:
  Intel Corporation
  2200 Mission College Blvd.
  Santa Clara, CA  97052 

*/
#ifndef __AEP_H__
#define __AEP_H__
#include <linux/types.h>

/**
List of all AEP return codes
*/
typedef enum {
   AEP_SUCCESS                              =  0,   
   AEP_ERROR_OPERATION_FAILED               =  1,
   AEP_ERROR_PCI_DEVICE_ACCESS              =  2, 
   AEP_ERROR_PCI_DEVICE_NOT_FOUND           =  3, 
   AEP_FAIL                                 =  4,
   AEP_INVALID_INPUT                        =  5,
   AEP_OUT_OF_MEMORY                        =  6, 
   AEP_ERROR_ALREADY_INITIALIZED            =  7,
   AEP_ERROR_FEATURE_NOT_IMPLEMENTED        =  8,
   AEP_ERROR_INVALID_PARAMETER              =  9,
   AEP_ERROR_NOT_ENABLED                    =  10,
   AEP_ERROR_NULL_POINTER                   = 25,
   AEP_ERROR_INVALID_REQUEST                = 26,
   AEP_ERROR_OUT_OF_RANGE                   = 27,
   AEP_ERROR_NOT_DONE                       = 28,
   AEP_ERROR_SUSPENDED                      = 29, /* Low-power state */
   AEP_ERROR_NO_RESOURCES                    = 30,
   
   AEP_LAST_NORMAL_ERROR_MARKER,

   AEP_ERROR_UNSPECIFIED                    = 99
} aep_result_t;

/* 
 * Read from GPIO register .
 * 
 * aep_gpio_read(unsigned int reg_offset, unsigned int* value)
 *
 * Description:
 *  This API is used to read GPIO register when the AEP is active.
 *
 * Precondition:
 *  AEP must be active
 *
 * Parameters:
 *  reg_offset   - GPIO  register offset to read from.
 *  value        - [output], the read value.
 *
 * Return:
 *  0 on Success, <0 on error.
 */   

aep_result_t aep_gpio_read( uint32_t gpio_address_offset, uint32_t* reg_value);

/* 
 * Write to GPIO register.
 * 
 * int aep_gpio_write(unsigned int reg_offset, unsigned int bit num, unsigned int value)
 *
 * Description:
 *  This API is used to write to a GPIO register when the AEP is active.
 *
 * Precondition:
 *  AEP must be active
 *
 * Parameters:
 *  reg_offset   - GPIO  register offset to read from.
 *  bit num        - The mask or bit num where value needs to be updated
 *  config         - Value to be written
 *
 * Return:
 *  0 on Success, <0 on error.
 */
aep_result_t aep_gpio_write( uint32_t gpio_address_offset, uint32_t bit_num, uint32_t gpio_config);

/* 
 * Write to GPIO register.

 * 
 * int aep_gpio_write(unsigned int reg_offset, unsigned int bit num, unsigned int value)
 *
 * Description:
 *  This API is used to write to a GPIO register when the AEP is active.

 *
 * Precondition:
 *  AEP must be active
 *
 * Parameters:

 *  reg_offset   - GPIO  register offset to read from.
 *  bit num        - The mask or bit num where value needs to be updated
 *  config         - Value to be written
 *
 * Return:
 *  0 on Success, <0 on error.
 */
aep_result_t aep_gpio_write_no_lock( uint32_t gpio_address_offset, uint32_t bit_num, uint32_t gpio_config);


/* 
 * Get AEP F/W Version.
 * 
 * int aep_get_fw(void)
 *
 * Description:
 *  This API is used to read the AEP F/W version 
 *
 * Precondition:
 *  This API can be used only if the AEP is active.
 *
 * Parameters:
 *  None.
 *
 * Return:
 *  AEP F/W version
 */

aep_result_t aep_get_fw(uint32_t* fw_ver);

/*
* Get AEP status
* bool aep_is_active()
*
* @brief The API returns if AEP is active on platform

* Parameters:
*  None.
*
* Return:
* true if active otherwise false
*/
bool aep_is_active(void);

#endif





